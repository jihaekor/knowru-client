# Knowru Client
---

**knowru-client** provides a client-side Python wrapper to use the Knowru API.

## Main Features
---

Below are some of the basic functions that knowru-client aims to handle:

- One-time authentication to persist across all Knowru operations.
- Knowledge creation using existing scikit-learn models.
- Run uploaded knowledges with provided input.
- Receive output from previous (and current) knowledge runs.

## Installation Instructions
---

The source code is currently hosted on BitBucket at: 
http://bitbucket.org/jihaekor/knowru-client/

Easiest way to install is to clone the repository into a folder of your choice. Use the following command:
    
```sh
git clone https://bitbucket.org/jihaekor/knowru-client/
```

## Dependencies
---

- [Requests](http://docs.python-requests.org): 2.6.0 or higher
- [scikit-learn](http://scikit-learn.org): 0.15.0 or higher

### Highly Recommended Dependencies
- [PyYAML](http://pyyaml.org)
    - Needed to load configuration files for ID and password from a YML configuration file

## Usage Instructions
---

First, you can get started by creating an instance of KnowruClient:
```python
knowru = KnowruClient(knowru_id, knowru_pw)
```

You can then use the instance to create a knowledge:
```python
knowledge_id = knowru.create_knowledge(knowledge_name, knowledge_description, sklearn_model)
```

Your knowledge will be saved in the `knowledge_id` variable returned in the function.

To run the knowledge with input, you need to know the ID that it is saved under. Once you have this information, you can use the `run_knowledge_with_input` function.
For example, to run the sample knowledge that is already uploaded, you can use:
```python
knowledge_id, results = knowru.run_knowledge_with_input(knowledge_id=1, data=[1, 2, 3, 4])
```

Your results will be saved in the `results` variable in the above example.

You can also refer to the example files in the `examples` folder for additional details.