# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 19:08:19 2015

@author: Jihae

Contains the KnowruClient class that enables linkage with the Knowru API.
"""

import json
import os
import re
import requests
import shutil
import tempfile
import unicodedata
import warnings
from sklearn.externals import joblib

class KnowruClient:
    """This is a class to connect to the Knowru API.
    
    Allows for saving models and for pulling results from saved models.
    
    It uses the requests package.
    """
    def __init__(self, knowru_id, pw, use_demo=True, root_url=None):
        """Initializes an instance of the Knowru client.
        
        Make sure to keep your ID and password a secret by pulling them from other 
        configuration files or from environment variables.
        
        URL also must end with /api/v1/ -- otherwise, you will get an error.
        
        Args:
            knowru_id (str): ID provided when Knowru was first set up.
            pw (str): Password for the ID.
            use_demo (bln, optional): True if using the demo website (demo.knowru.com); Defaults to True.
            root_url (str, optional): Root API URL; defaults to [knowru_id].knowru.com/api/v1 if not provided.
        """
        # Save away the values
        self.knowru_id = knowru_id
        self.pw = pw
        
        # Construct the URL.
        # If the demo site, use demo.knowru.com
        if use_demo:
            root_url = 'http://demo.knowru.com/api/v1/'
        elif root_url is None:
            root_url = 'http://%s.knowru.com/api/v1/'%(knowru_id)
        else:
            # Use the provided root_url if not given
            pass
        
        # Save away the URL.
        # First, add a '/' if it does not end with one
        if root_url[-1] != '/':
            root_url += '/'
            
        # Second, test to see that the url ends with '/api/v1/'
        assert root_url[-8:] == '/api/v1/', "The Root URL must end with '/api/v1/'"
        
        # Finally, save
        self.root_url = root_url
        
        # Create a new Requests session to handle all API input/output
        self.session = requests.Session()
        
        # Authenticate the session
        self.session_authenticate()
        
        # Get the root information -- a dictionary of folders that you can navigate to.
        self.get_root_information()
        
    def grab_latest_csrf_token(self, url):
        """Returns the latest CSRF Token to be used in POST
        
        Args:
            url (str): URL to send POST request on
        
        Returns:
            String with CSRF Token
            
        """
        # Save away the variable locally
        s = self.session
        
        # Get the CSRF Token
        s.get(url)
        csrftoken = s.cookies['csrftoken']
        
        return csrftoken        
        
    def session_authenticate(self):
        """This authenticates the session.
        """
        # Save away the variable locally
        s = self.session
        
        # Generate the login URL
        login_url = '%s/api-auth/login/'%(self.root_url[:-8])
        
        # CSRF Token
        csrftoken = self.grab_latest_csrf_token(login_url)
        
        # Post to the Login Page
        login = {'username': self.knowru_id, 'password': self.pw, 
                 'csrfmiddlewaretoken': csrftoken, 'next': '/api/v1/'}
        s.post(login_url, data=login, headers={'Referer': login_url})
        
        # Save back the session
        self.session = s
        
        return self
        
    def get_root_information(self):
        """Gets the folder structure from the root.
        """
        # Create a request to the root        
        r = self.session.get(self.root_url)
        
        # Save away the resulting dictionary of folder structure
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()
        
        self.folders = r.json()
        
        return self
        
    def get_knowledge_by_id(self, knowledge_id):
        """Get Knowledge settings by ID
        
        Args:
            knowledge_id (int): Knowledge ID
        
        Returns:
            Results in JSON
            
        """
        # Save away the session locally
        s = self.session
            
        # Let's create the GET URL
        url = '%s%d/'%(self.folders['knowledges'], knowledge_id)
        
        # Get the results
        r = s.get(url)
        
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()
        
        # Get the response
        rjson = r.json()
        
        # Return the summary information
        return rjson
        
    def add_knowledge_file(self, knowledge_id, file_path):
        """Posts a knowledge file to a given knowledge ID
        
        Args:
            knowledge_id (int): Knowledge ID
            file_path (str): Path to the file to upload
        
        """
        # Save away the session locally
        s = self.session
        
        # Let's create the POST JSON for Knowledge File
        # Knowledge Value to Pass is the URL
        knowledge_url = '%s%d/'%(self.folders['knowledges'], knowledge_id)
        url = self.folders['knowledge-files']
        
        # Get the CSRF Token
        csrftoken = self.grab_latest_csrf_token(url)
        
        # Create the payload and headers
        file_name = file_path.split('/')[-1].split('\\')[-1]
        payload = {'knowledge': knowledge_url, 'csrfmiddlewaretoken': csrftoken}
        files = {'file': (file_name, open(file_path, 'rb'))}
        headers = {'Referer': url}
        
        # Make the request
        r = s.post(url, data=payload, files=files, headers=headers)
        
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()        
        
        # Get the response and save away the id
        rjson = r.json()
        knowledge_file_id = rjson['id']
        
        return knowledge_file_id
        
    def create_knowledge(self, name, description, model=None, output_folder=None):
        """Posts the given model as a knowledge file.
        
        Args:
            name (str): Name to give knowledge
            description (str): Knowledge description
            model (scikit-learn model, optional): Scikit-learn model to persist
            output_folder (str, optional): Output folder to use. Otherwise, it creates a temporary directory.
        
        Returns:
            Knowledge ID and API URL
            
        """
        # Save away the session locally
        s = self.session
        
        # Create the directory that will be used.
        if output_folder is None:
            folder_to_use = tempfile.mkdtemp()
        else:
            folder_to_use = output_folder
        
        # Let's create the Knowledge first.
        # Let's create the POST JSON for Knowledge
        url = self.folders['knowledges']
        
        # Get the CSRF Token
        csrftoken = self.grab_latest_csrf_token(url)
        
        # Create the payload and headers
        payload = {'name': name, 'description': description, 'csrfmiddlewaretoken': csrftoken}
        headers = {'Referer': url}
        
        # Make the request
        r = s.post(url, data=payload, headers=headers)
        
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()
        
        # Get the response and save away the id
        rjson = r.json()
        knowledge_id = rjson['id']
        
        # If the model is provided, let's take the model and dump to disk.
        # Then, we can upload each file as a Knowledge file
        # List to hold the file_id's
        knowledge_file_id_list = []
        if model is not None:
            # Use the provided knowledge name to create the file name
            # Make sure the file name is friendly.
            file_name = unicodedata.normalize('NFKD', u'%s'%(name)).encode('ascii', 'ignore').decode('ascii')
            file_name = re.sub('[^\w\s-]', '', file_name).strip()
            file_name += '.pkl'
            
            # Dump the file
            dumped_files = joblib.dump(model, os.path.join(folder_to_use, file_name))
            
            # Make into a list if not already
            if not isinstance(dumped_files, list):
                dumped_files = [dumped_files]
                
            for f in dumped_files:
                knowledge_file_id = self.add_knowledge_file(knowledge_id, f)
                knowledge_file_id_list.append(knowledge_file_id)
                
            # Remove the temporary directory when finished
            if output_folder is None:
                shutil.rmtree(folder_to_use, ignore_errors=True)
        
        # Return the IDs
        return knowledge_id, knowledge_file_id_list
        
    def run_knowledge_with_input(self, knowledge_id, data):
        """Runs the knowledge by passing through an input.
        
        The input must be in a list format.
        
        Args:
            knowledge_id (int): Knowledge ID
            data (list): Data to be used as input
        
        Returns:
            Output results
        
        """
        # Save away the session locally
        s = self.session
        
        # Create the data into a string of lists.
        try:
            if not isinstance(data, list):
                data = list(data)
        except Exception:
            raise ValueError('The data could not be transformed into a list.')
        else:
            # Format to string
            data = ['%f'%(x) for x in data]
            data_string = '[%s]'%(', '.join(data))
        
        # Let's create the knowledge run.
        knowledge_url = '%s%d/'%(self.folders['knowledges'], knowledge_id)
        url = self.folders['runs']
        
        # Get the CSRF Token
        csrftoken = self.grab_latest_csrf_token(url)
        
        # Create the payload and headers
        payload = {'knowledge': knowledge_url, 'input_json_data': data_string, 
                   'csrfmiddlewaretoken': csrftoken}
        headers = {'Referer': url}
        
        # Make the request
        r = s.post(url, data=payload, headers=headers)
        
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()        
        
        # Get the response and save away the id
        rjson = r.json()
        knowledge_run_id = rjson['id']
        
        # Get the status and results
        status = rjson['status']
        error_type = rjson['error_type']
        error_msg = rjson['error_msg']        
        
        # Check status for success
        if status != 'SUCCESS':
            warnings.warn('The output result contains an error and results will be blank. ' \
                          'Error Type: %s; Message: %s'%(error_type, error_msg))
        
        # Grab the results
        try:
            results = json.loads(rjson['output_json_data'])
        except TypeError:
            results = None
        
        return knowledge_run_id, results
        
    def get_run_output_by_id(self, run_id):
        """Get Output by Run ID.
        
        Raises an error if there was an error in the output
        
        Args:
            run_id (int): Run ID
        
        Returns:
            Results in List
            
        """
        # Save away the session locally
        s = self.session
            
        # Let's create the GET URL
        url = '%s%d/'%(self.folders['runs'], run_id)
        
        # Get the results
        r = s.get(url)
        
        # Check the status code -- You Want a Successful Code
        r.raise_for_status()
        
        # Get the response
        rjson = r.json()
        
        # Get the status and results
        status = rjson['status']
        error_type = rjson['error_type']
        error_msg = rjson['error_msg']        
        
        # Check status for success
        assert status == 'SUCCESS', 'The output result contains an error: %s; Message: %s'%(error_type, error_msg)
        
        # Grab the results
        results = json.loads(rjson['output_json_data'])
        
        # Return the results
        return results