# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 19:59:37 2015

@author: Jihae

Series of unit tests for the KnowruClient
"""

import os
import yaml
import unittest
import requests
import datetime as dt
from functools import partial

from knowru_client import KnowruClient

class KnowruClientTest(unittest.TestCase):
    def setUp(self):
        # Get ID and Password from config file
        with open(os.path.join(os.path.dirname(__file__), '../analytics/source/credentials/knowru_configs.yml'), 'r') as f:
            configs = yaml.load(f)
        
        # Save back to instance
        self.configs = configs
        
        return self
    
    def tearDown(self):
        pass
    
    def set_up_knowru(self, user=None, pw=None):
        """Sets up the KnowruClient
        
        Args:
            user (str, optional): Username; Defaults to config file
            pw (str, optional): Password; Defaults to config file
        
        """
        # Get user and pw if None
        if user is None:
            user = self.configs['id']
        
        if pw is None:
            pw = self.configs['pw']
            
        self.knowru = KnowruClient(user, pw)
        
        return self
        
    
    def test_incorrect_credentials(self):
        # If incorrect credentials are given, should let the user know.
        # Try creating KnowruClient with incorrect credenntials
        set_up_knowru_incorrect = partial(self.set_up_knowru, pw='')
        self.assertRaises(requests.exceptions.HTTPError, set_up_knowru_incorrect)
    
    def test_root_site_connection(self):
        # Create KnowruClient
        self.set_up_knowru()
        
        # Connection to the root site should return a list of three folders:
        # (1) knowledges, (2) knowledge-files, (3) runs
        folders = self.knowru.folders        
        self.assertEqual(sorted(folders.keys()), 
                         sorted(['knowledges', 'knowledge-files', 'runs']))
                         
    def test_knowledge_create(self):
        # Create KnowruClient
        self.set_up_knowru()
        
        # Creating the knowledge file should return the Knowledge ID
        KNOWLEDGE_NAME = 'Unit Test on %s'%(dt.datetime.now().strftime('%Y%m%d%H%M%S'))
        knowledge_id, __ = self.knowru.create_knowledge(KNOWLEDGE_NAME, 'Unit Testing')
        
        # Test to see if the id returned is an integer
        self.assertIsInstance(knowledge_id, int)
        
        # We should then be able to get the URL using the ID
        knowledge_list = self.knowru.get_knowledge_by_id(knowledge_id)
        
        self.assertEqual(knowledge_list['name'], KNOWLEDGE_NAME)
        self.assertEqual(knowledge_list['description'], 'Unit Testing')
        
    def test_knowledge_run(self):
        # Create KnowruClient
        self.set_up_knowru()
        
        # We can use the sample already uploaded. This will always have ID 1
        __, results = self.knowru.run_knowledge_with_input(1, [1, 2, 3, 4])
        
        self.assertEqual(results, [2])
        
        
if __name__ == '__main__':
    # Run the unit tests
    unittest.main()