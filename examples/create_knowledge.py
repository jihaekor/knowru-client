# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 22:49:49 2015

@author: Jihae
"""

import os
import sys
import yaml
import datetime as dt
from sklearn.externals import joblib

# Global configurations for imports
directory = os.path.dirname(__file__)
sys.path.append(os.path.join(directory, '..'))

from knowru_client import KnowruClient

if __name__ == '__main__':
    # Testing Scripts
    # Get ID and Password from config file
    with open(os.path.join(directory, '../../analytics/source/credentials/knowru_configs.yml'), 'r') as f:
        configs = yaml.load(f)
        
    # Create KnowruClient
    knowru = KnowruClient(configs['id'], configs['pw'])
    
    # A sample scikit-learn model...
    clf = joblib.load(os.path.join(directory, '../../analytics/futures/model_instances/NG/best_model.joblib.pkl'))['model']
    
    # Let's try to create a knowledge
    knowledge_name = 'Test on %s'%(dt.datetime.now().strftime('%Y%m%d%H%M%S'))
    knowledge_id, knowledge_file_id_list = knowru.create_knowledge(knowledge_name, 'Description Test', clf)
    
    knowledge_data = knowru.get_knowledge_by_id(knowledge_id)