# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 22:16:15 2015

@author: Jihae

KnowruClient Connection Example
"""

import os
import sys
import yaml

# Global configurations for imports
directory = os.path.dirname(__file__)
sys.path.append(os.path.join(directory, '..'))

from knowru_client import KnowruClient

if __name__ == '__main__':
    # Testing Scripts
    # Get ID and Password from config file
    with open(os.path.join(directory, '../../analytics/source/credentials/knowru_configs.yml'), 'r') as f:
        configs = yaml.load(f)
        
    # Create KnowruClient
    knowru = KnowruClient(configs['id'], configs['pw'])