# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 00:24:37 2015

@author: Jihae
"""

import os
import sys
import yaml

# Global configurations for imports
directory = os.path.dirname(__file__)
sys.path.append(os.path.join(directory, '..'))

from knowru_client import KnowruClient

if __name__ == '__main__':
    # Testing Scripts
    # Get ID and Password from config file
    with open(os.path.join(directory, '../../analytics/source/credentials/knowru_configs.yml'), 'r') as f:
        configs = yaml.load(f)
        
    # Create KnowruClient
    knowru = KnowruClient(configs['id'], configs['pw'])
            
    # Let's try to run a knowledge
    # Use the sample knowledge on the server.
    knowledge_run_id, results = knowru.run_knowledge_with_input(1, [1, 2, 3])